package com.tsystems.javaschool.tasks.calculator;

public class CalcException extends Exception {
    public CalcException(String s) {
        super(s);
    }
}
