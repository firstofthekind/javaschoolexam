package com.tsystems.javaschool.tasks.calculator;

import java.util.*;

public class Calculator {

    private static final String operations = "+-*/";
    private static final String bracks = "() ";
    private static final Map<String, Integer> prior;
    private static final Set<String> delim;
    private static final Set<String> oper;
    static {
        prior = new HashMap<>();
        delim = new HashSet<>();
        oper = new HashSet<>();
        prior.put("(", 1);
        prior.put("-", 2);
        prior.put("+", 2);
        prior.put("*", 3);
        prior.put("/", 3);
        for (int i = 0; i < bracks.length(); i++) {
            delim.add("" + bracks.charAt(i));
        }
        for (int i = 0; i < operations.length(); i++) {
            oper.add("" + operations.charAt(i));
        }
    }

    private static boolean isDelimiter(String str) {
        return delim.contains(str);
    }

    private static boolean isOperator(String str) {
        return oper.contains(str);
    }

    private static int priority(String str) throws CalcException {
        int priority = prior.get(str);
        if (str == null)
            throw new CalcException("wrong priority item");
        return priority;
    }

    private static TokenType typeRecognizer(String str) {
        if (str.equals(" "))
            return TokenType.SPACE;
        if (isOperator(str))
            return TokenType.OPERATOR;
        if (isDelimiter(str))
            return TokenType.DELIMITER;
        return TokenType.OPERAND;
    }

    private enum TokenType{
        SPACE,
        OPERAND,
        OPERATOR,
        DELIMITER
    }

    public static List<String> parse(String str) throws CalcException {
        List<String> strOut = new ArrayList<>();
        Deque<String> stack = new ArrayDeque<>();
        StringTokenizer tokenizer = new StringTokenizer(str, bracks + operations, true);
        String prev = "";
        String curr;
        while (tokenizer.hasMoreTokens()) {
            curr = tokenizer.nextToken();
            switch (typeRecognizer(curr)) {
                case SPACE:
                    continue;

                case OPERAND:
                    strOut.add(curr);
                    break;

                case DELIMITER:
                    if (curr.equals("(")) stack.push(curr);
                    else if (curr.equals(")")) {
                        while (true) {
                            assert stack.peek() != null;
                            if (stack.peek().equals("(")) break;
                            strOut.add(stack.pop());
                            if (stack.isEmpty()) {
                                throw new CalcException("Parentheses unpaired");
                            }
                        }
                        stack.pop();
                    }
                    break;

                case OPERATOR:
                    if (!tokenizer.hasMoreTokens()) {
                        throw new CalcException("Incorrect syntax");
                    }
                    if (curr.equals("-") && (prev.equals("") || (isDelimiter(prev)  && !prev.equals(")")))) {
                        strOut.add("0");
                    }
                    else {
                        while (!stack.isEmpty() && (priority(curr) <= priority(stack.peek()))) {
                            strOut.add(stack.pop());
                        }

                    }
                    stack.push(curr);
                    break;
            }
            if (isOperator(prev) && isOperator(curr))
                throw new CalcException("two operands in a row operand");
            prev = curr;
        }

        while (!stack.isEmpty()) {
            if (isOperator(stack.peek())) strOut.add(stack.pop());
            else {
                throw new CalcException("Parentheses unpaired");
            }
        }
        return strOut;
    }

    public static Double calc(List<String> str) throws CalcException {
        Deque<Double> stack = new ArrayDeque<>();
        double a, b;
        for (String x : str) {
            switch (x) {
                case "+":
                    stack.push(stack.pop() + stack.pop());
                    break;

                case "-":
                    b = stack.pop();
                    a = stack.pop();
                    stack.push(a - b);
                    break;

                case "*":
                    stack.push(stack.pop() * stack.pop());
                    break;

                case "/":
                    b = stack.pop();
                    if (b == 0)
                        throw new CalcException("division by null");
                    a = stack.pop();
                    stack.push(a / b);
                    break;

                default:
                    try {
                        stack.push(Double.parseDouble(x));
                        break;
                    } catch (NumberFormatException e) {
                        throw new CalcException("Element parse error");
                    }
            }
        }
        return stack.pop();
    }

    public String evaluate(String str) {
        if (str == null || str.equals(""))
            return null;
        String stringResult;
        try {
            Double doubleResult = calc(parse(str));
            if (doubleResult == Math.floor(doubleResult)) {
                stringResult = String.valueOf(doubleResult.intValue());
            } else {
                stringResult = String.valueOf(doubleResult);
            }
        } catch (CalcException cpe) {
            System.out.println(cpe.getMessage());
            return null;
        }
        return stringResult;
    }


}
