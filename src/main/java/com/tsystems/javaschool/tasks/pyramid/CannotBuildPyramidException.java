package com.tsystems.javaschool.tasks.pyramid;

public class CannotBuildPyramidException extends Exception {
    public CannotBuildPyramidException(String s) {
        super(s);
    }

    public CannotBuildPyramidException() {

    }
}
